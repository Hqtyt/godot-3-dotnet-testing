using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class Game : Control
{
	private CenterContainer IntroductionContainer;
	private CenterContainer ShopContainer;
	private CenterContainer BattleContainer;

	private List<string> PlayerInventory = new List<string>();


	public override void _Ready()
	{
		IntroductionContainer = (CenterContainer)GetNode("Introdcution");
		ShopContainer = (CenterContainer)GetNode("Shop");
		BattleContainer = (CenterContainer)GetNode("Battle");
	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (!@event.IsPressed())
		{
			return;
		}

		if (!ShopContainer.Visible)
		{
			Progress();
			return;
		}

		bool itemPurchased = false;
		for (int i = 1; i <= 9; i++)
		{
			if (!@event.IsActionPressed(i.ToString()))
			{
				continue;
			}

			Button shopButton; // = (Button)ShopContainer.GetNode(i.ToString());
			Godot.Collections.Array containerChildren = ShopContainer.GetNode("Items").GetChildren();
			GD.Print("Shopping------------------");
			GD.Print(containerChildren);
			GD.Print("Count:", ShopContainer.GetNode("Items").GetChildCount());

			for (int childi = 0; childi < ShopContainer.GetNode("Items").GetChildCount(); childi++)
			{
				CanvasItem child = (CanvasItem)containerChildren[childi];
				GD.Print(child.Name);
				GD.Print(child.Name.Contains(i.ToString()));
				GD.Print("itomatch: ", i.ToString());

				if (child.Name.Contains(i.ToString()))
				{
					shopButton = (Button)child;
					PlayerInventory.Add(shopButton.Text.Split(" - ")[1]);
					itemPurchased = true;

					break;
				}
			}

			if (itemPurchased)
			{
				break;
			}
		}
		GD.Print("Shopping------------------");

		if (!itemPurchased)
		{
			// move to dedicated Label node and toggle visibility ???
			GD.Print("You decided not to purchase any items. Continue?");
			// On player confirmation => Progress()
			Progress();
		}
		else
		{
			GD.Print("PlayerInventory: ");
			foreach (string item in PlayerInventory)
			{
				GD.Print(item);
			}
		}
	}

	private void Progress()
	{
		if (IntroductionContainer.Visible)
		{
			SwitchContainers(IntroductionContainer, ShopContainer);
		}
		else if (ShopContainer.Visible && !ProgressContainer(ShopContainer))
		{
			SwitchContainers(ShopContainer, BattleContainer);
		}
		else if (BattleContainer.Visible && !ProgressContainer(BattleContainer))
		{
			SwitchContainers(BattleContainer, ShopContainer);
		}
	}

	private void SwitchContainers(CenterContainer containerFrom, CenterContainer containerTo)
	{
		containerFrom.Visible = false;
		containerTo.Visible = true;

		CanvasItem firstChild = (CanvasItem)containerTo.GetChild(0);
		firstChild.Visible = true;
	}

	private bool ProgressContainer(CenterContainer container)
	{
		bool progressed = false;

		Godot.Collections.Array containerChildren = container.GetChildren();

		for (int i = 0; i < container.GetChildCount(); i++)
		{
			CanvasItem child = (CanvasItem)containerChildren[i];

			if (child.Visible)
			{
				child.Visible = false;

				if (i < container.GetChildCount() - 1)
				{
					CanvasItem nextChild = (CanvasItem)containerChildren[i + 1];

					nextChild.Visible = true;
					progressed = true;

					break;
				}
			}
		}

		return progressed;
	}
}
